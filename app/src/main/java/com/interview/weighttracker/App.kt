package com.interview.weighttracker

import android.app.Application
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import com.google.firebase.FirebaseApp
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber
import java.lang.Exception

@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        //for testing purposes
        //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        FirebaseApp.initializeApp(this)
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Firebase.crashlytics.setCrashlyticsCollectionEnabled(false)
        }else{
            Timber.plant(CrashReportingTree())
        }
    }

    private class CrashReportingTree : Timber.Tree() {

        override fun log(
            priority: Int,
            tag: String?,
            message: String,
            throwable: Throwable?,
        ) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return
            }
            val t = throwable ?: Exception(message)

            // Crashlytics
            val crashlytics = Firebase.crashlytics
            crashlytics.setCustomKey(CRASHLYTICS_KEY_PRIORITY, priority)
            crashlytics.setCustomKey(CRASHLYTICS_KEY_MESSAGE, message)
            if(tag != null) crashlytics.setCustomKey(CRASHLYTICS_KEY_TAG, tag)

            crashlytics.recordException(t)

        }

        companion object {
            private const val CRASHLYTICS_KEY_PRIORITY = "priority"
            private const val CRASHLYTICS_KEY_TAG = "tag"
            private const val CRASHLYTICS_KEY_MESSAGE = "message"
        }
    }




}