package com.interview.weighttracker

import android.content.res.Resources
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.utils.ViewPortHandler
import com.interview.weighttracker.data.model.RecordedWeight
import com.interview.weighttracker.ui.weight.RecordedWeightUI
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class ChartUtil(private val graph : LineChart) {
    companion object {
        val GRAPH_MESSAGE_CONSTANT = ":::GRAPH:::"

        fun convertDataToString(weightList : List<RecordedWeightUI>) : String{
            if(weightList.isEmpty()) return ""

            val sb = StringBuilder()
            sb.append(GRAPH_MESSAGE_CONSTANT)
            for(w in weightList){
                sb.append(w.weight)
                    .append(",")
                    .append(w.dateWeighed)
                    .append(",")
                    .append(w.dateInserted)
                    .append("\n")
            }
            return sb.toString()
        }

    }
    //region graph methods

    fun createGraph(weightList : String){
        if(weightList.isEmpty()) return

        val list = ArrayList<RecordedWeightUI>()
        val str = weightList.replace(GRAPH_MESSAGE_CONSTANT, "")

        for(data in str.split("\n")){
            val weightTimes = data.split(",")
            if(weightTimes.size > 2) {
                list.add(RecordedWeightUI(RecordedWeight("",
                    weightTimes[0].toDouble(),
                    weightTimes[1].toLong(),
                    weightTimes[2].toLong())))
            }
        }

        createGraph(list)
    }

    fun createGraph(weightList : List<RecordedWeightUI>) {
        val weightListNew = weightList.toList().sortedBy { it.dateWeighed }.map {  //to list for deepcopy
            val days = it.dateWeighedInDays
            val weight = it.weight
            val dateInserted = it.dateInserted
            RecordedWeightUI(RecordedWeight("", weight, days, dateInserted))
        }

        styleGraph()
        val lineData = LineData(createDataSet(weightListNew))
        if (weightList.isEmpty()) {
            graph.clear()
        } else {
            graph.data = lineData
        }
        graph.invalidate()
    }

    private fun generateEntries(weightList : List<RecordedWeightUI>): ArrayList<Entry> {
        val entries = ArrayList<Entry>()
        for (w in weightList) {
            val dateWeighed = w.dateWeighed.toFloat()
            val weight = w.weight.toFloat()

            entries.add(Entry(dateWeighed, weight, w.dateInserted))
        }

        return entries
    }

    private fun createDataSet(weightList : List<RecordedWeightUI>): LineDataSet {
        val dataSet = LineDataSet(generateEntries(weightList), "Weight")
        dataSet.apply {
            color = ContextCompat.getColor(graph.context, R.color.purple_200)
            setCircleColor(ContextCompat.getColor(graph.context, R.color.purple_200))
            setDrawValues(true)
            valueTextSize = 12f
            valueTextColor = ContextCompat.getColor(graph.context,R.color.graph_labels_color)
            valueFormatter = WeightValueFormatter(graph.context.resources)
        }

        return dataSet
    }

    private fun styleGraph() {
        //Axis
        graph.xAxis.apply {
            valueFormatter = MyXAxisValueFormatter()
            textSize = 12f
            textColor = ContextCompat.getColor(graph.context,R.color.graph_labels_color)
            axisLineColor = ContextCompat.getColor(graph.context,R.color.graph_labels_color)
            granularity = 2f
            labelCount = 3
            position = XAxis.XAxisPosition.BOTTOM
            setDrawGridLines(false)
        }

        graph.axisRight.isEnabled = false


        graph.axisLeft.apply {
            valueFormatter = MyYAxisValueFormatter(graph.context.resources)
            textSize = 14f
            granularity = 2f
            labelCount = 6
            textColor = ContextCompat.getColor(graph.context,R.color.graph_labels_color)
            axisLineColor = ContextCompat.getColor(graph.context,R.color.graph_labels_color)
            setDrawAxisLine(true)
            setDrawZeroLine(true)
            setDrawGridLines(false)
        }



        //Functional
        graph.apply {
            setTouchEnabled(true)
            setScaleEnabled(true)
            setPinchZoom(true)
            isDoubleTapToZoomEnabled = true

            //visual
            setNoDataTextColor(ContextCompat.getColor(graph.context, R.color.white))
            setDrawGridBackground(false)
            setDrawBorders(false)
            val desc = Description()
            desc.text = ""
            description = desc
            legend.isEnabled = false
        }
    }


    class  MyXAxisValueFormatter : IAxisValueFormatter {
        override fun getFormattedValue(value: Float, axis: AxisBase?): String {
            val sdf = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault()) //TODO get rid of local
            val dateInMilli = TimeUnit.DAYS.toMillis(value.toLong())
            return sdf.format(Date(dateInMilli))
        }

    }

    class  MyYAxisValueFormatter(val resources: Resources) : IAxisValueFormatter {
        override fun getFormattedValue(value: Float, axis: AxisBase?): String {
            return resources.getString(R.string.weight_y_axis_format_graph, value)
        }

    }

    class  WeightValueFormatter(val resources: Resources) : IValueFormatter {
        override fun getFormattedValue(
            value: Float,
            entry: Entry?,
            dataSetIndex: Int,
            viewPortHandler: ViewPortHandler?,
        ): String {
            return resources.getString(R.string.weight_format_graph, value)
        }


    }

    //endregion
}