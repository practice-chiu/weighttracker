package com.interview.weighttracker

import android.content.Context
import android.content.res.Resources
import android.util.TypedValue
import android.widget.Toast
import androidx.annotation.StringRes
import com.github.mikephil.charting.data.Entry
import com.interview.weighttracker.data.model.Resource
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


//TODO make a toast with an application context
fun Context.toast(@StringRes message : Int) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
fun Context.toast(message: CharSequence) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
fun Context.toast(res: Resource<*>, defaultMessage : String) {
    val message = if(res.message.isNullOrEmpty()) defaultMessage else res.message
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}
fun Context.toast(res: Resource<*>, @StringRes defaultMessage : Int) {
    val message = if(res.message.isNullOrEmpty()) this.getString(defaultMessage) else res.message
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}


fun Int.dpToPx(resources: Resources) : Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), resources.displayMetrics).toInt()
}

fun Long.backendDateToDate() : String{
    val outputFormatter = SimpleDateFormat("MM/dd/yy", Locale.getDefault())
    return outputFormatter.format(Date(this))
}

fun Long.backendDateToHoursAndMin() : String{
    val outputFormatter = SimpleDateFormat("hh:mm aa", Locale.getDefault())
    return outputFormatter.format(Date(this))
}

fun Entry.getId() : String{
    return this.y.toString() + TimeUnit.DAYS.toMillis(this.x.toLong()).toString()  + this.data.toString()
}


