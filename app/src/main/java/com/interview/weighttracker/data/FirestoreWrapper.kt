package com.interview.weighttracker.data

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.toObject
import com.interview.weighttracker.data.model.*
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.first
import timber.log.Timber


@OptIn(ExperimentalCoroutinesApi::class)
class FirestoreWrapper(private val firestore : FirebaseFirestore, val auth : FirebaseAuth) {

    companion object  {
        const val WEIGHT_TRACKED_COLLECTION = "weightTracked"
        const val MESSAGES_COLLECTION = "messages"
    }
    //holding state and thats bad...
    //region weight tracking methods
    var recordedWeights : RecordedWeights? = null

    suspend fun getRecordedWeights() : Flow<Resource<MutableList<RecordedWeight>>> = callbackFlow {

        val userId = auth.currentUser?.uid ?: ""

        if(userId.isEmpty()) {
            cancel(CancellationException("User not logged in"))
            close()
            return@callbackFlow
        }

        val subscription =  firestore.collection(WEIGHT_TRACKED_COLLECTION).document(userId).addSnapshotListener(
            object : EventListener<DocumentSnapshot> { // better for debugging as one can see the class variables

                override fun onEvent(snapshot: DocumentSnapshot?, e: FirebaseFirestoreException?) {
                    if(e != null){
                        Timber.e(e, "listen Failed")
                        trySend(Resource.Error("Error Getting document", null, e))
                    }else if (snapshot != null && snapshot.exists()) {
                        recordedWeights = snapshot.toObject<RecordedWeights>()

                        Timber.d("Current data: ${snapshot.data}")
                        trySend(
                            Resource.Success(convertRecordedWeightsToRecordedWeight(userId,recordedWeights))
                        )
                    } else {
                        recordedWeights = RecordedWeights(userId, emptyList(), emptyList(), emptyList())
                        Timber.d("Current data: null")
                        trySend(Resource.Success(ArrayList()))
                    }
                }
            })
        awaitClose{ subscription.remove() }

    }

    //TODO remove duplicate code on bottom / add callbackflow eventually
    suspend fun updateRecordWeight(weight: Double, dateWeighed: Long, dateInserted: Long) {
        val userId = auth.currentUser?.uid ?: ""

        if(userId.isEmpty()) {
            return
        }

        if(recordedWeights == null) {
            getRecordedWeights().first()
        }

        if(recordedWeights == null){
            return
        }else {
            val recordedWeightsLocal = recordedWeights!!
            val weightList: MutableList<Double> = ArrayList(recordedWeightsLocal.weightList)
            val dateWeighedList: MutableList<Long> = ArrayList(recordedWeightsLocal.dateWeighedList)
            val dateInsertedList: MutableList<Long> = ArrayList(recordedWeightsLocal.dateInsertedList)

            weightList.add(weight)
            dateWeighedList.add(dateWeighed)
            dateInsertedList.add(dateInserted)

            recordedWeights = RecordedWeights(
                userId,
                weightList,
                dateWeighedList,
                dateInsertedList
            )

            firestore.collection(WEIGHT_TRACKED_COLLECTION).document(userId).set(recordedWeights!!, SetOptions.merge())
                .addOnSuccessListener {
                    Timber.d("DocumentSnapshot successfully written!")
                }
                .addOnFailureListener { e ->
                    Timber.w(e, "Error writing document")
                }
        }
    }

    //TODO duplicate code from top method / add callbackflow eventually
    suspend fun removeRecordedWeight(id : String) {
        val userId = auth.currentUser?.uid ?: ""

        if(userId.isEmpty()) {
            return
        }

        if(recordedWeights == null) {
            getRecordedWeights().first()
        }

        if(recordedWeights == null){
            return
        }else {
            val recordedWeightsLocal = recordedWeights!!
            val weightList: MutableList<Double> = ArrayList(recordedWeightsLocal.weightList)
            val dateWeighedList: MutableList<Long> = ArrayList(recordedWeightsLocal.dateWeighedList)
            val dateInsertedList: MutableList<Long> = ArrayList(recordedWeightsLocal.dateInsertedList)

            var indexToRemove : Int? = null
            for(i in weightList.indices){
                val rW = RecordedWeight(userId,weightList[i], dateWeighedList[i], dateInsertedList[i])
                if(id == rW.id){
                    indexToRemove = i
                }
            }

            if(indexToRemove == null){
                return
            }

            weightList.removeAt(indexToRemove)
            dateWeighedList.removeAt(indexToRemove)
            dateInsertedList.removeAt(indexToRemove)

            recordedWeights = RecordedWeights(
                userId,
                weightList,
                dateWeighedList,
                dateInsertedList
            )

            firestore.collection(WEIGHT_TRACKED_COLLECTION).document(userId).set(recordedWeights!!, SetOptions.merge())
                .addOnSuccessListener {
                    Timber.d("DocumentSnapshot successfully written!")
                }
                .addOnFailureListener { e ->
                    Timber.w(e, "Error writing document")
                }
        }

    }
    //endregion

    //region messaging secion
    var messages : Messages? = null

    suspend fun getMessages() : Flow<Resource<MutableList<Message>>> = callbackFlow {

        val userId = auth.currentUser?.uid ?: ""

        if(userId.isEmpty()) {
            cancel(CancellationException("User not logged in"))
            close()
            return@callbackFlow
        }

        val subscription =  firestore.collection(MESSAGES_COLLECTION).document(userId).addSnapshotListener(
            object : EventListener<DocumentSnapshot> { // better for debugging as one can see the class variables

                override fun onEvent(snapshot: DocumentSnapshot?, e: FirebaseFirestoreException?) {
                    if(e != null){
                        Timber.e(e, "listen Failed")
                        trySend(Resource.Error("Error Getting document", null, e))
                    }else if (snapshot != null && snapshot.exists()) {
                        messages = snapshot.toObject<Messages>()

                        Timber.d("Current data: ${snapshot.data}")
                        trySend(
                            Resource.Success(convertMessagesToMessage(messages))
                        )
                    } else {
                        messages = Messages(emptyList(),emptyList(), emptyList(), emptyList())
                        Timber.d("Current data: null")
                        trySend(Resource.Success(ArrayList()))
                    }
                }
            })
        awaitClose{ subscription.remove() }

    }

    //TODO remove duplicate code on bottom / add callbackflow eventually
    suspend fun sendMessage(message: String, dateSent: Long) {
        val userId = auth.currentUser?.uid ?: ""

        if(userId.isEmpty()) {
            return
        }

        val messagesMethodLocal = messages
        if(messagesMethodLocal == null) {
            getMessages().first()
        }

        if(messagesMethodLocal == null){
            return
        }else {
            val messageList = ArrayList(messagesMethodLocal.userMessageList)
            val dateSentList = ArrayList(messagesMethodLocal.userDateSentList)

            messageList.add(message)
            dateSentList.add(dateSent)

            messages = Messages(
                messageList,
                dateSentList,
                messagesMethodLocal.expertMessageList,
                messagesMethodLocal.expertDateSentList
            )

            firestore.collection(MESSAGES_COLLECTION).document(userId).set(messages!!, SetOptions.merge())
                .addOnSuccessListener {
                    Timber.d("DocumentSnapshot successfully written!")
                }
                .addOnFailureListener { e ->
                    Timber.w(e, "Error writing document")
                }
        }
    }
    //endregion

    //region private methods
    private fun convertRecordedWeightsToRecordedWeight(userId : String, recordedWeights: RecordedWeights?) : MutableList<RecordedWeight> {
        val list = ArrayList<RecordedWeight>()
        if(recordedWeights == null){
            return list
        }

        for(i in recordedWeights.dateInsertedList.indices){
            list.add(
                RecordedWeight(
                    userId,
                    recordedWeights.weightList[i],
                    recordedWeights.dateWeighedList[i],
                    recordedWeights.dateInsertedList[i]
                )
            )
        }

        list.sortByDescending { it.dateWeighed }
        return list
    }

    private fun convertMessagesToMessage(messages: Messages?) : MutableList<Message> {
        val list = ArrayList<Message>()
        if(messages == null){
            return list
        }

        for(i in messages.userMessageList.indices){
            list.add(
                Message(
                    messages.userDateSentList[i],
                    messages.userMessageList[i],

                )
            )
        }

        for(i in messages.expertDateSentList?.indices ?: emptyList()){
            list.add(
                Message(
                    dateSent = messages.expertDateSentList!![i],
                    expertMessage = messages.expertMessageList!![i],
                )
            )
        }

        list.sortBy { it.dateSent }
        return list
    }
    //endregion
}