package com.interview.weighttracker.data.model

class Message(
    val dateSent : Long,
    val userMessage : String? = null,
    val expertMessage : String? = null
    )