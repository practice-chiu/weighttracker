package com.interview.weighttracker.data.model

class Messages(
    val userMessageList : List<String>,
    val userDateSentList : List<Long>,
    val expertMessageList : List<String>? = null,
    val expertDateSentList : List<Long>? = null
    ) {
    constructor() : this(emptyList(), emptyList()) //needed for firebase toObject method
}