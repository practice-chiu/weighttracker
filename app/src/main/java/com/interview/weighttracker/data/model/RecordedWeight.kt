package com.interview.weighttracker.data.model

data class RecordedWeight (
    val userId: String,
    val weight: Double,
    val dateWeighed : Long,
    val dateInserted : Long
    ){
    val id = weight.toString() + dateWeighed.toString() + dateInserted.toString()
}