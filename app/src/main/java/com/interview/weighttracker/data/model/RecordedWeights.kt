package com.interview.weighttracker.data.model

data class RecordedWeights (
    val userId: String,
    val weightList : List<Double>,
    val dateWeighedList: List<Long>,
    val dateInsertedList : List<Long>
    ){
    constructor() : this("", ArrayList(), ArrayList(),ArrayList()) //needed for firebase toObject method
}