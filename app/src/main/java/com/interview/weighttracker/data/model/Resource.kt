package com.interview.weighttracker.data.model

sealed class Resource<T>(
    val data: T? = null,
    val message: String? = null,
    val messageLocal : Int? = null,
    val error : Throwable? = null
) {
    class Success<T>(val successData: T) : Resource<T>(successData)
    class Loading<T>(data: T? = null) : Resource<T>(data)
    class Initial<T>(data: T? = null) : Resource<T>(data)
    class Error<T>(
        message: String? = null,
        messageLocal : Int? = null,
        error: Throwable? = null,
        data: T? = null
    ) : Resource<T>(data, message, messageLocal, error)
}