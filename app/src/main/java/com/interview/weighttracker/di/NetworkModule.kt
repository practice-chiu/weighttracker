package com.interview.weighttracker.di

import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.interview.weighttracker.R
import com.interview.weighttracker.data.FirestoreWrapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import java.text.SimpleDateFormat
import java.util.*

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Provides
    fun providesGoogleSignInClient(@ApplicationContext context: Context): GoogleSignInClient {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(context.getString(R.string.android_client_id))
            .requestEmail()
            .build()

        return GoogleSignIn.getClient(context, gso)
    }

    @Provides
    fun providesFirebaseAuth() : FirebaseAuth {
        return Firebase.auth
    }

    @Provides
    fun providesFirebaseCrashlytics() : FirebaseCrashlytics {
        return FirebaseCrashlytics.getInstance()
    }


    @Provides
    fun providesFirestoreWrapper(auth : FirebaseAuth) : FirestoreWrapper {
        return FirestoreWrapper(FirebaseFirestore.getInstance(), auth)
    }

    @Provides
    fun providesIODispatcher() : CoroutineDispatcher {
        return Dispatchers.IO
    }


    @Provides
    fun providesValidDateFormat(@ApplicationContext context: Context) : SimpleDateFormat {
        return SimpleDateFormat(context.getString(R.string.date_format_validity), Locale.getDefault())
    }

}