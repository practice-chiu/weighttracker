package com.interview.weighttracker.ui.chat

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

class ChatAdapter : ListAdapter<MessageUI, ChatViewHolder>(diffCallback) {
    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        return ChatViewHolder(parent)
    }

    companion object {

        val diffCallback = object : DiffUtil.ItemCallback<MessageUI>() {
            override fun areItemsTheSame(oldItem: MessageUI, newItem: MessageUI): Boolean {
                return  oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: MessageUI, newItem: MessageUI): Boolean {
                return oldItem == newItem
            }
        }
    }
}