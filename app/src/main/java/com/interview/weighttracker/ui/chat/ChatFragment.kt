package com.interview.weighttracker.ui.chat

import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.interview.weighttracker.ChartUtil
import com.interview.weighttracker.R
import com.interview.weighttracker.data.model.Resource
import com.interview.weighttracker.databinding.FragmentChatBinding
import com.interview.weighttracker.dpToPx
import com.interview.weighttracker.toast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ChatFragment : Fragment() {

    private val vm: ChatViewModel by viewModels()
    private val adapter = ChatAdapter()
    private var weightStr = ""

    private var _binding: FragmentChatBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        _binding = FragmentChatBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val rv = binding.chatRv

        //region rv
        val divider = DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
        val drawable = GradientDrawable()
        drawable.setSize(0, 3.dpToPx(resources))
        divider.setDrawable(drawable)
        rv.addItemDecoration(divider)
        (rv.layoutManager as LinearLayoutManager).stackFromEnd = true
        rv.adapter = adapter
        //endregion

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {

                launch {
                    vm.messageList.collectLatest {
                        if(it !is Resource.Success){
                            return@collectLatest
                        }

                        adapter.submitList(it.data)

                    }
                }
                launch {
                    vm.weightList.collectLatest {
                        if(it !is Resource.Success){
                            return@collectLatest
                        }

                        weightStr = ChartUtil.convertDataToString(it.successData)
                    }
                }
            }
        }

        binding.chatMessage.addTextChangedListener {
            binding.chatSend.isEnabled = !it.isNullOrEmpty()
        }

        binding.chatSend.setOnClickListener {
            val msg = binding.chatMessage.text.toString()
            if(msg.isEmpty()) return@setOnClickListener
            vm.sendMessage(msg, System.currentTimeMillis())
            binding.chatMessage.setText("")
        }
        binding.chatWeightGraphSend.setOnClickListener {
            if(weightStr.isNotEmpty()){
                vm.sendMessage(weightStr, System.currentTimeMillis())
            }else{
                requireContext().toast(R.string.chat_send_weight_error)
            }
        }

        adapter.registerAdapterDataObserver(object: RecyclerView.AdapterDataObserver() {
            // for some reason on initialization this causes our list not to be on the bottom
            //thus we'll disable on first run then enable it
            var isFirstTime = true
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                if(!isFirstTime) binding.chatRv.scrollToPosition(positionStart)
                isFirstTime = false
            }

        })


        return root
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}