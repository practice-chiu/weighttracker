package com.interview.weighttracker.ui.chat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.charts.LineChart
import com.interview.weighttracker.ChartUtil
import com.interview.weighttracker.R

class ChatViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context)
        .inflate(R.layout.view_chat_message_item, parent, false)
) {
    private val chatMessageContainer = itemView.findViewById<View>(R.id.item_chat_message_container)
    private val chatExpertContainer = itemView.findViewById<View>(R.id.item_chat_expert_container)
    private val chatExpertMessage = itemView.findViewById<TextView>(R.id.item_chat_expert)

    private val chatUserContainer = itemView.findViewById<View>(R.id.item_chat_user_container)
    private val chatUserMessage = itemView.findViewById<TextView>(R.id.item_chat_user)

    private val headerContainer = itemView.findViewById<View>(R.id.item_chat_date_header_container)
    private val dateHeader = itemView.findViewById<TextView>(R.id.item_chat_date)

    private val graph = itemView.findViewById<LineChart>(R.id.item_weight_line_chart)

    private val chartUtil = ChartUtil(graph)

    fun bindTo(item: MessageUI) {
        //setting visibility
        if(item.headerDate != null){
            chatMessageContainer.visibility = View.GONE
            graph.visibility = View.GONE
            headerContainer.visibility = View.VISIBLE
        }else {
            chatMessageContainer.visibility = View.VISIBLE
            headerContainer.visibility = View.GONE
            if(item.userMessage != null && !item.userMessage.contains(ChartUtil.GRAPH_MESSAGE_CONSTANT)){
                chatUserContainer.visibility = View.VISIBLE
                chatExpertContainer.visibility = View.GONE
                graph.visibility = View.GONE
            }else if(item.userMessage != null && item.userMessage.contains(ChartUtil.GRAPH_MESSAGE_CONSTANT)){
                chatUserContainer.visibility = View.GONE
                chatExpertContainer.visibility = View.GONE
                graph.visibility = View.VISIBLE
            }
            else{
                chatUserContainer.visibility = View.GONE
                chatExpertContainer.visibility = View.VISIBLE
                graph.visibility = View.GONE
            }
        }

        //setting data
        chatExpertMessage.text = item.expertMessage
        chatUserMessage.text = item.userMessage
        dateHeader.text = item.headerDate

        chartUtil.createGraph(item.userMessage ?: "")

    }
}