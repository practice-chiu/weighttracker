package com.interview.weighttracker.ui.chat

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.interview.weighttracker.backendDateToDate
import com.interview.weighttracker.data.FirestoreWrapper
import com.interview.weighttracker.data.model.Resource
import com.interview.weighttracker.ui.weight.RecordedWeightUI
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChatViewModel @Inject constructor(
    private val ioDispatcher: CoroutineDispatcher,
    private val firestoreWrapper: FirestoreWrapper
): ViewModel() {

    private val _messageList = MutableStateFlow<Resource<List<MessageUI>>>(Resource.Initial())
    val messageList: StateFlow<Resource<List<MessageUI>>> get() = _messageList

    private val _weightList = MutableStateFlow<Resource<List<RecordedWeightUI>>>(Resource.Initial())
    val weightList: StateFlow<Resource<List<RecordedWeightUI>>> get() = _weightList

    init {
        viewModelScope.launch {
            firestoreWrapper.getMessages().map {
                when (it) {
                    is Resource.Success -> {
                        val list = ArrayList<MessageUI>()
                        for(i in it.successData){
                            list.add(MessageUI(i))
                        }
                        val returnList = addEventUIHeaders(list)
                        Resource.Success(returnList)
                    }
                    is Resource.Error -> {
                        Resource.Error(it.message, it.messageLocal, it.error)
                    }
                    else -> {
                        Resource.Loading()
                    }
                }
            }.collectLatest {
                _messageList.value = it
            }
        }

        viewModelScope.launch {
            firestoreWrapper.getRecordedWeights().map { //duplicate code
                when (it) {
                    is Resource.Success -> {
                        val list = ArrayList<RecordedWeightUI>()
                        for(i in it.successData){
                            list.add(RecordedWeightUI(i))
                        }
                        Resource.Success(list.toList())
                    }
                    is Resource.Error -> {
                        Resource.Error(it.message, it.messageLocal, it.error)
                    }
                    else -> {
                        Resource.Loading()
                    }
                }
            }.collectLatest {
                _weightList.value = it
            }
        }
    }


    fun sendMessage(message: String, dateSent : Long) {
        viewModelScope.launch(ioDispatcher) {
            firestoreWrapper.sendMessage(
                message.trim(),
                dateSent
            )
        }

    }

    private fun addEventUIHeaders(list : List<MessageUI>) : List<MessageUI>{
        val returnList = ArrayList<MessageUI>()
        for(i in list.indices){
            val messageUI = list[i]

            if(i == 0 || messageUI.dateSentMilli.backendDateToDate() != list[i-1].dateSentMilli.backendDateToDate()){
                returnList.add(MessageUI(messageUI.message,
                    messageUI.dateSentMilli.backendDateToDate()))
            }
            returnList.add(messageUI)
        }
        return returnList
    }



}