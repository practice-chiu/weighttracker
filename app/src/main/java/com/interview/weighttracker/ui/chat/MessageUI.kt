package com.interview.weighttracker.ui.chat

import com.interview.weighttracker.backendDateToHoursAndMin
import com.interview.weighttracker.data.model.Message

data class MessageUI(val message: Message, val headerDate : String? = null){
    val id = message.dateSent
    val userMessage = message.userMessage
    val expertMessage = message.expertMessage
    val dateSent = message.dateSent.backendDateToHoursAndMin()
    val dateSentMilli = message.dateSent
}
