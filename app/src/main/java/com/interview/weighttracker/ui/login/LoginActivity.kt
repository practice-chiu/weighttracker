package com.interview.weighttracker.ui.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.interview.weighttracker.MainActivity
import com.interview.weighttracker.R
import com.interview.weighttracker.data.model.Resource
import com.interview.weighttracker.databinding.ActivityLoginBinding
import com.interview.weighttracker.toast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    private var _binding: ActivityLoginBinding? = null
    private val binding get() = _binding!!

    val vm : LoginViewModel by viewModels()

    //TODO clean up class fine for now
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //TODO a bit hacky should be encapsulated in the viewmodel
        val resultLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()) { result ->

            if (result.resultCode == Activity.RESULT_OK) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(result.data)
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    val account = task.getResult(ApiException::class.java)!!
                    Timber.d("firebaseAuthWithGoogle:" + account.id)
                    vm.login(account.idToken!!)
                } catch (e: ApiException) {
                    Timber.w(e, "Google sign in failed")
                    vm.cancelledLogin()
                    this@LoginActivity.toast(R.string.login_failed)
                }
            }else{
                binding.loginBlock.blockingProgressBar.visibility = View.GONE
                vm.cancelledLogin()
            }
        }

        binding.signInButton.setSize(SignInButton.SIZE_WIDE)
        binding.signInButton.setOnClickListener {
            vm.login(resultLauncher)
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                vm.isUserLoggedIn.collectLatest { isUserLoggedIn ->
                    if (isUserLoggedIn is Resource.Loading) {
                        binding.loginBlock.blockingProgressBar.visibility = View.VISIBLE
                        return@collectLatest
                    }

                    binding.loginBlock.blockingProgressBar.visibility = View.GONE
                    if (isUserLoggedIn.data != null && isUserLoggedIn.data) {
                        startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                        finish()
                    } else if (isUserLoggedIn is Resource.Error) {
                        this@LoginActivity.toast(isUserLoggedIn, R.string.login_failed)
                    }
                }
            }
        }
    }
}
