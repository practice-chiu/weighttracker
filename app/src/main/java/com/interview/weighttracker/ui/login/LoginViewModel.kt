package com.interview.weighttracker.ui.login

import android.content.Intent
import androidx.activity.result.ActivityResultLauncher
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.interview.weighttracker.data.model.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.Executors
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val firebaseAuth: FirebaseAuth,
    private val googleSignInClient: GoogleSignInClient,
    private val firebaseCrashlytics: FirebaseCrashlytics //TODO all of this should be in a repo
    ) : ViewModel() {

    private val _isUserLoggedIn : MutableStateFlow<Resource<Boolean>> =
        MutableStateFlow(Resource.Initial(firebaseAuth.currentUser != null))
    val isUserLoggedIn get() = _isUserLoggedIn

    init {
        viewModelScope.launch { //TODO clean this up
            isUserLoggedIn.collectLatest {
                if (firebaseAuth.currentUser != null) {
                    firebaseCrashlytics.setUserId(firebaseAuth.currentUser!!.uid)
                }
            }
        }
    }

    fun cancelledLogin(){
        _isUserLoggedIn.value = Resource.Initial()
    }

    fun login(resultLauncher : ActivityResultLauncher<Intent>){
        if(isUserLoggedIn.value is Resource.Loading){
            return
        }

        _isUserLoggedIn.value = Resource.Loading()
        val signInIntent = googleSignInClient.signInIntent
        resultLauncher.launch(signInIntent)
    }


    fun login(idToken : String){
        viewModelScope.launch {
            _isUserLoggedIn.value = Resource.Loading()
            val credential = GoogleAuthProvider.getCredential(idToken, null)
            firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(Executors.newSingleThreadExecutor()) { task -> // TODO look into executors/see if this causes a bug
                    if (task.isSuccessful) {
                        Timber.d("signInWithCredential:success")
                        _isUserLoggedIn.value = Resource.Success(firebaseAuth.currentUser != null)
                    } else {
                        Timber.d("signInWithCredential:failure", task.exception)
                        _isUserLoggedIn.value =
                            Resource.Error(task.exception?.message ?: "", null, task.exception)
                    }
                }
        }
    }

}