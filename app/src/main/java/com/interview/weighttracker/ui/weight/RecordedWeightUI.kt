package com.interview.weighttracker.ui.weight

import com.interview.weighttracker.data.model.RecordedWeight
import java.util.concurrent.TimeUnit

data class RecordedWeightUI(val recordedWeight: RecordedWeight) {
    val id = recordedWeight.id
    val weight = recordedWeight.weight
    val dateWeighed = recordedWeight.dateWeighed
    val dateInserted = recordedWeight.dateInserted
    val dateWeighedInDays = TimeUnit.MILLISECONDS.toDays(dateWeighed)
    val idForChart = this.weight.toString() + dateWeighedInDays.toString()  + this.dateInserted.toString()
}