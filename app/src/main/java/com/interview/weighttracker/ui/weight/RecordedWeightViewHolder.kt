package com.interview.weighttracker.ui.weight

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.interview.weighttracker.R
import java.text.SimpleDateFormat
import java.util.*

class RecordedWeightViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context)
        .inflate(R.layout.view_recorded_weight_item, parent, false)
) {
    lateinit var record: RecordedWeightUI

    private val weightTV = itemView.findViewById<TextView>(R.id.view_record_weight_weight)
    private val dateTV = itemView.findViewById<TextView>(R.id.view_record_weight_date)

    fun bindTo(UI: RecordedWeightUI) {
        record = UI
        weightTV.text = itemView.context.getString(R.string.weight_format, record.weight)
        dateTV.text = SimpleDateFormat(itemView.context.getString(R.string.date_format_item), Locale.getDefault())
            .format(Date(record.dateWeighed))

    }
}