package com.interview.weighttracker.ui.weight

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

class WeightAdapter : ListAdapter<RecordedWeightUI, RecordedWeightViewHolder>(diffCallback) {
    override fun onBindViewHolder(holder: RecordedWeightViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecordedWeightViewHolder {
        return RecordedWeightViewHolder(parent)
    }

    companion object {

        val diffCallback = object : DiffUtil.ItemCallback<RecordedWeightUI>() {
            override fun areItemsTheSame(oldUI: RecordedWeightUI, newUI: RecordedWeightUI): Boolean {
                return  oldUI.id == newUI.id
            }

            override fun areContentsTheSame(oldUI: RecordedWeightUI, newUI: RecordedWeightUI): Boolean {
                return oldUI == newUI
            }
        }
    }
}