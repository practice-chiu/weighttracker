package com.interview.weighttracker.ui.weight

import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.*
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.interview.weighttracker.*
import com.interview.weighttracker.data.model.Resource
import com.interview.weighttracker.databinding.FragmentWeightBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.util.*


@AndroidEntryPoint
class WeightFragment : Fragment() {

    private val vm: WeightViewModel by activityViewModels()

    private val adapter = WeightAdapter()
    lateinit var graph : LineChart
    lateinit var chartUtil : ChartUtil
    private var _binding: FragmentWeightBinding? = null
    private val binding get() = _binding!!

    //region lifecycle
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        setHasOptionsMenu(true)

        _binding = FragmentWeightBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val rv = binding.weightRv

        graph = binding.weightLineChart
        chartUtil = ChartUtil(graph)
        //region rv
        val divider = DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
        val drawable = GradientDrawable()
        drawable.setSize(0, 3.dpToPx(resources))
        divider.setDrawable(drawable)
        rv.addItemDecoration(divider)
        rv.adapter = adapter

        initSwipeToDelete()
        //endregion

        graph.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onValueSelected(e: Entry, h: Highlight) {
                var posToScroll = -1
                for(i in adapter.currentList.indices){
                    if(adapter.currentList[i].idForChart == e.getId()){
                        posToScroll = i
                        break
                    }
                }

                if(posToScroll != -1) {
                    (rv.layoutManager as LinearLayoutManager).scrollToPosition(posToScroll)

                }
            }

            override fun onNothingSelected() {}
        })

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {

                launch {
                    vm.weightList.collectLatest {
                        binding.weightBlocking?.blockingProgressBar?.visibility = View.GONE
                        if(it !is Resource.Success){
                            if(it is Resource.Loading){
                                binding.weightBlocking?.blockingProgressBar?.visibility = View.VISIBLE
                            }

                            return@collectLatest
                        }

                        val list = it.successData
                        if(list.isNotEmpty()) {
                            binding.weightContainer?.visibility = View.VISIBLE
                            binding.weightEmptyView?.visibility = View.GONE
                            val mostRecentWeight = list[0].weight
                            binding.weightMostRecentWeight.text =
                                getString(R.string.weight_format, mostRecentWeight)
                        }else{
                            binding.weightContainer?.visibility = View.GONE
                            binding.weightEmptyView?.visibility = View.VISIBLE
                        }

                        adapter.submitList(list)
                        chartUtil.createGraph(list)
                    }
                }
            }

        }
        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.weight_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_add_weight -> {
                findNavController().navigate(R.id.navigation_weight_add)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    //endregion

    //region private methods
    private fun initSwipeToDelete() {
        ItemTouchHelper(object : ItemTouchHelper.Callback() {
            // enable the items to swipe to the left or right
            override fun getMovementFlags(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
            ): Int {
                val vH = viewHolder as RecordedWeightViewHolder
                return if (vH.record != null) {
                    makeMovementFlags(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT)
                } else {
                    makeMovementFlags(0, 0)
                }
            }

            override fun onMove(
                recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder,
            ): Boolean = false

            // When an item is swiped, remove the item via the view model. The list item will be
            // automatically removed in response, because the adapter is observing the live list.
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                (viewHolder as RecordedWeightViewHolder).record?.let {
                    vm.remove(it.id)
                }
            }
        }).attachToRecyclerView(binding.weightRv)

    }

    //endregion

}