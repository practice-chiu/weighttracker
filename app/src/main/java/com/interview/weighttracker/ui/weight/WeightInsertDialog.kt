package com.interview.weighttracker.ui.weight

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.interview.weighttracker.R
import com.interview.weighttracker.databinding.FragmentWeightInsertBinding
import com.interview.weighttracker.toast
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class WeightInsertDialog : BottomSheetDialogFragment() {


    private val vm: WeightViewModel by activityViewModels()

    private var _binding: FragmentWeightInsertBinding? = null
    private val binding get() = _binding!!


    //region lifecycle

    override fun onStart() {
        super.onStart()
        //this needs to be done for landscape mode
        val behavior = BottomSheetBehavior.from(requireView().parent as View)
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View {

        _binding = FragmentWeightInsertBinding.inflate(inflater, container, false)
        val root: View = binding.root


        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.weightEnterDate.setText(
            SimpleDateFormat(getString(R.string.date_format_validity), Locale.getDefault()).format(Date(System.currentTimeMillis()))
        )

        binding.weightConfirmButton.setOnClickListener {
            val weightEntered = binding.weightEnterWeight.text.toString()
            val datedEntered = binding.weightEnterDate.text.toString()

            val errorMsg = vm.insert(weightEntered, datedEntered, System.currentTimeMillis())
            if(errorMsg != null){
                context?.toast(errorMsg)
                return@setOnClickListener
            }
            dismiss()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    //endregion

}