package com.interview.weighttracker.ui.weight

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.interview.weighttracker.R
import com.interview.weighttracker.data.FirestoreWrapper
import com.interview.weighttracker.data.model.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.text.ParseException
import java.text.SimpleDateFormat
import javax.inject.Inject

@HiltViewModel
class WeightViewModel @Inject constructor(
    private val ioDispatcher: CoroutineDispatcher,
    private val firestoreWrapper: FirestoreWrapper,
    private val validDateFormat: SimpleDateFormat
) : ViewModel() {

    private val _weightList = MutableStateFlow<Resource<List<RecordedWeightUI>>>(Resource.Initial())
    val weightList: StateFlow<Resource<List<RecordedWeightUI>>> get() = _weightList

    init {
        viewModelScope.launch {
            firestoreWrapper.getRecordedWeights().map {
                when (it) {
                    is Resource.Success -> {
                        val list = ArrayList<RecordedWeightUI>()
                        for(i in it.successData){
                            list.add(RecordedWeightUI(i))
                        }
                        Resource.Success(list.toList())
                    }
                    is Resource.Error -> {
                        Resource.Error(it.message, it.messageLocal, it.error)
                    }
                    else -> {
                        Resource.Loading()
                    }
                }
            }.collectLatest {
                _weightList.value = it
            }
        }
    }


    fun insert(weight: String, dateEntered: String, dateInserted : Long) : Int? {
        val message = validateEnteredDateAndWeight(weight, dateEntered)
        if(message != null){
            return message
        }

        val dateEnteredMilli = validDateFormat.parse(dateEntered)?.time

        if(dateEnteredMilli != null) {
            viewModelScope.launch(ioDispatcher) {
                firestoreWrapper.updateRecordWeight(
                    weight.toDouble(),
                    dateEnteredMilli,
                    dateInserted
                )
            }
        }
        return message
    }

    fun remove(id: String){
        viewModelScope.launch(ioDispatcher) {
            firestoreWrapper.removeRecordedWeight(id)
        }
    }


    //region methods

    private fun validateEnteredDateAndWeight(weightEntered: String, dateEntered: String) : Int? {
        if(weightEntered.isEmpty() && dateEntered.isEmpty()){
            return R.string.weight_date_entered_empty
        }

        if(weightEntered.isEmpty()){
            return R.string.weight_weight_empty
        }

        if(dateEntered.isEmpty()){
            return R.string.weight_date_empty
        }

        if(weightEntered.toBigDecimalOrNull() == null){
            return R.string.weight_format_invalid
        }

        if(dateEntered.length != 10){
            return R.string.date_format_invalid
        }

        try {
            if(validDateFormat.parse(dateEntered) == null){
                return R.string.date_format_invalid
            }
        }catch (e : ParseException){
            return R.string.date_format_invalid
        }


        return null
    }

    //endregion

}