package com.interview.weighttracker.ui.weight

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.interview.weighttracker.MainCoroutineRule
import com.interview.weighttracker.data.FirestoreWrapper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.text.SimpleDateFormat
import java.util.*
import kotlin.test.assertTrue

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(AndroidJUnit4::class)
class WeightViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()


    private val firestoreWrapper : FirestoreWrapper = mock()

    @Test
    fun validityOfWeightRecordInputTest() = runBlockingTest {

        whenever(firestoreWrapper.getRecordedWeights()).thenReturn( flow {  })

        val weightViewModel = WeightViewModel(
            mainCoroutineRule.dispatcher,
            firestoreWrapper,
            SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())
        )
        var error = weightViewModel.insert(".", ".", 0L)
        assertTrue(error != null)

        error = weightViewModel.insert("5", "10", 0L)
        assertTrue(error != null)

        error = weightViewModel.insert("5", "10/18", 0L)
        assertTrue(error != null)


        error = weightViewModel.insert("5", "10/18/91", 0L)
        assertTrue(error != null)


        error = weightViewModel.insert("5", "1/18/91", 0L)
        assertTrue(error != null)

        error = weightViewModel.insert("5", "10/18/1991", 0L)
        assertTrue(error == null)

    }
}